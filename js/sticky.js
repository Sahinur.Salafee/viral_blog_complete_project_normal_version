$(document).ready(function() {
      
        var stickyNavTop = $('.navbar-dark').offset().top;

        var stickyNav = function () {

          var scrollTop = $(window).scrollTop();
              
          if (scrollTop > stickyNavTop) { 
              $('.navbar-dark').addClass('sticky');
          } else {
              $('.navbar-dark').removeClass('sticky'); 
          }
      };

      $(window).on('load scroll', function() {
        stickyNav();
      });
});