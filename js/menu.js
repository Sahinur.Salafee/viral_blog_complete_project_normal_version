$(window).on('load resize', function () {
	if ($(window).width() <= 1199) {

		return;
	} else {
		$('.nav-item > .nav-link').siblings('ul').removeAttr('style');
	}
});

$('.nav-item > .nav-link, .sub-menu > a').on('click', function () {
	if ($(window).width() >= 1199) {

		return;
	} else {
		$(this).siblings('ul').slideToggle(300);
	}
});

//modal js

var signIn = $('#sign-in');
var signUpPage = $('#sign-up-page');

signIn.on('click', function () {
	signUpPage.modal('hide');
});