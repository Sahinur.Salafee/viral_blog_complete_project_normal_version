  // === Masonry blog posts ===
$(window).on("load", function() {
  "use strict";


  var triggerM = $('.masonry-trigger');
  var container = $('.articles-wrapper');

  function startMasonry() {
    container.masonry({
      gutter: 0,
      itemSelector: '.article',
      columnWidth: '.article',
      isInitLayout: true
    });
  }
  if (triggerM.css("display") == "block") {
    startMasonry();
  }
  $(window).resize(function() {
    if (triggerM.css("display") == "block") {
      startMasonry();
    } else if (triggerM.css("display") == "none") {
      startMasonry();
      container.masonry('destroy');
    }
  });
  });