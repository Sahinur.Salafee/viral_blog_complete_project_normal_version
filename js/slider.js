$(document).on('ready', function() {
  $(".gallery-slider").slick({
    lazyLoad: 'ondemand',
    autoplay: true,
    arrows: false,
    dots: false,
    infinite: true
  });


   $(".home-layout-three-slider").slick({
    lazyLoad: 'ondemand',
    autoplay: true,
    arrows: false,
    dots: false,
    infinite: true,
  	autoplaySpeed: 2000
  });


   $(".home-layout-five-slider").slick({
    lazyLoad: 'ondemand',
    autoplay: true,
    arrows: false,
    dots: false,
    infinite: true,
  	autoplaySpeed: 2000
  });


   $(".home-layout-two-slider").slick({
    lazyLoad: 'ondemand',
    autoplay: true,
    arrows: false,
    dots: true,
    infinite: true,
  	autoplaySpeed: 2000
  });

});